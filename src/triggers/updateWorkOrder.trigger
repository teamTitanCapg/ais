trigger updateWorkOrder on Case (after insert, after Update) {
    
    if(Trigger.IsInsert && Trigger.IsAfter)
         caseTriggerHandler.createWorkOrder(Trigger.new);
    
    if(Trigger.IsUpdate && Trigger.IsAfter)
         caseTriggerHandler.createAIrcraftKnowledgeSolution(Trigger.new);
}