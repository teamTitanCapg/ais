trigger MapEventValues on Flying_Fridge_Event__e (after insert) {
    
    Integer optimalHighPressure = 500;
    Integer optimalLowPressure = 300;
    List<Case> caseList = new List<Case>();
    for(Flying_Fridge_Event__e evt : Trigger.New){
        if(evt.Pressure_Level__c >=optimalHighPressure  || evt.Pressure_Level__c <=optimalLowPressure ){
            Case cobj = new Case();
            cobj.Aircraft_Pressure_Level__c = evt.Pressure_Level__c;
            cobj.Subject = 'Created by IOT Salesforce';
            caseList.add(cobj);
        }    
    }
    
    Insert caseList;
}