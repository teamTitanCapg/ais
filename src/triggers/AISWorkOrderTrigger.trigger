trigger AISWorkOrderTrigger on WorkOrder (after insert) {

    if (Trigger.isAfter && Trigger.isInsert) 
           workOrderTriggerHandler.allocateExpertTechnician(Trigger.new);
    
}