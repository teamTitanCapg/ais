<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Creation_email</fullName>
        <description>Case Creation email</description>
        <protected>false</protected>
        <recipients>
            <recipient>All Access</recipient>
            <type>caseTeam</type>
        </recipients>
        <recipients>
            <field>ContactId</field>
            <type>contactLookup</type>
        </recipients>
        <recipients>
            <recipient>shreya.barua@capgemini.com.innovation</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Flight_Incident_Case</template>
    </alerts>
    <rules>
        <fullName>Case Email Notification</fullName>
        <actions>
            <name>Case_Creation_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow to send email notification when case gets created</description>
        <formula>!ISBLANK( Subject )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
