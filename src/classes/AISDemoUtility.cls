public class AISDemoUtility{ 

    public static void fslCustomSchedulingAndServiceAlloc(List<workorder> workorderlist,Id schedulingPolicyId, Timezone tz)
    {
                    OperatingHours abOperatingHours = [SELECT Name, Id, (SELECT EndTime, StartTime, Type, DayOfWeek FROM TimeSlots) FROM OperatingHours WHERE Name = 'Emergency Operation Hours' limit 1];
                    list<ServiceAppointment> saLists = new list<ServiceAppointment>();
                    list<AssignedResource > resourceAllocations = new list<AssignedResource>();
                    
                    //Map<Id,List<FSL.AppointmentBookingSlot>> sAtoSlotMap = new Map<Id,List<FSL.AppointmentBookingSlot>>();
                    
                    for(workorder wo : workorderlist)
                    {
                        ServiceAppointment sa = new ServiceAppointment();
                        sa.EarliestStartTime = system.now();
                        sa.DueDate= system.now().addHours(9);
                        sa.ParentRecordId=wo.id;
                        saLists.add(sa);
                    }
                   
                    insert saLists;
                    
                    Id resourceId =[SELECT id,Name FROM ServiceResource WHERE Name='Aircraft Expert'].Id;
                    
                   
                    System.debug('@@@@ schedulingPolicyId '+schedulingPolicyId);
                    System.debug('@@@@ abOperatingHours '+abOperatingHours);
                    
                    
                    for(ServiceAppointment sa : saLists)
                    {
                       /*List<FSL.AppointmentBookingSlot> slots = FSL.AppointmentBookingService.GetSlots(sa.Id, schedulingPolicyId, abOperatingHours, tz,  'SORT_BY_DATE', false);
                       System.debug('@@@@ slots size'+ slots.size());
                       FSL.AppointmentBookingSlot slot = slots[0];*/
                       sa.ArrivalWindowStartTime = system.now();
                       sa.ArrivalWindowEndTime = system.now().addHours(2);
                   
                    }
                    
                    if(saLists !=null && saLists.size()>0)
                        update saLists;
                      
                   
                    
                       
                    for(ServiceAppointment sa : saLists)
                    {  
                       System.debug('@@@@ Before Scheduling');
                       FSL.ScheduleResult scheduleResult = FSL.ScheduleService.Schedule(schedulingPolicyId, sa.Id);
                       
                       if (scheduleResult != null)
                        {
                            System.debug('SA was scheduled successfully on: ' + scheduleResult.Service.SchedStartTime + ' GMT');
                            /*AssignedResource resourceAllocation = new AssignedResource();
                            resourceAllocation.ServiceAppointmentId = sa.Id;
                            resourceAllocation.ServiceResourceId = resourceId;
                            resourceAllocations.add(resourceAllocation);*/
                        }
                        else
                        {
                            System.debug('Service was not scheduled.');
                        }
                      
                    }
                    
                  
                   ///if(resourceAllocations !=null && resourceAllocations.size()>0)
                           //insert resourceAllocations;
                     
                  // System.debug('@@@@ resourceAllocations '+JSON.serializePretty(resourceAllocations)); 
               
    }


}