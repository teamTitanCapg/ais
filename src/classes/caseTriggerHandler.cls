public with sharing class caseTriggerHandler{ 
 public static final string WORKTYPE_NAME='Aircraft Emergency';
 public static final string SUBJECT='Work Order for Aircraft Technician';
 public static void createWorkOrder(List<Case> cases)
    {
       try{ 
        
            List<WorkOrder> workOrderList = new List<WorkOrder>();
            Id worktypeId = [SELECT Id FROM WorkType WHERE name=:WORKTYPE_NAME].Id;
            for(Case ca : cases){
                
                WorkOrder wobj = new WorkOrder();
                wobj.CaseId = ca.Id;
                wobj.Subject = SUBJECT;
                wobj.WorkTypeId= worktypeId ;
                workOrderList.add(wobj);
                
            }
            
            Insert workOrderList;
        
         }
         catch(Exception ex)
         {
            system.debug('Exception Occured-->'+ex.getMessage());
         }
    }
    
    public static void createAIrcraftKnowledgeSolution(List<Case> cases)
    {
      try
      {
          List<Knowledge__kav> kblist = new List<Knowledge__kav>();
          List<CaseArticle> caseArts = new List<CaseArticle>();
          //Id articleId = [SELECT DeveloperName,Id FROM RecordType WHERE SobjectType = 'LinkedArticle'].Id;
          
          for(Case ca : cases)
          {
             Knowledge__kav knowledge = new Knowledge__kav();
             knowledge.Summary=ca.Comments;
             knowledge.Language='en_US';
             knowledge.IsVisibleInPkb=true;
             knowledge.IsVisibleInPrm=true;
             knowledge.IsVisibleInCsp=true;
             knowledge.Title='Aicraft Technical Issues : ' + ca.subject;
             knowledge.UrlName='test';
             
             kblist.add(knowledge); 
          }
          
          insert kblist;
      
          for(Knowledge__kav kv : kblist)
          {
            CaseArticle caseArt = new CaseArticle();
            caseArt.KnowledgeArticleId = kv.id;
            caseArts.add(caseArt);
          }
          
          for(integer i=0;cases!=null && i<cases.size();i++)
             caseArts[i].CaseId = cases[i].id;
          
          
          insert caseArts;
          
          for(Knowledge__kav kv : kblist)
                KbManagement.PublishingService.publishArticle(kv.Id, false);
      }
      catch(Exception ex)
       {
            system.debug('Exception Occured-->'+ex.getMessage());
       }
    } 

}