public with sharing class workOrderTriggerHandler{

    public static void allocateExpertTechnician(List<workorder> workorderlist)
    {
        
        try
        { 
           
             if(workorderlist!=null && workorderlist.size()>0)
             {
                 Id schedulingPolicyId=[select id from FSL__Scheduling_Policy__c where Name='Emergency' limit 1].Id;
                 Timezone tz = UserInfo.getTimeZone();
                 AISDemoUtility.fslCustomSchedulingAndServiceAlloc(workorderlist,schedulingPolicyId,tz);
             }
         
        }
        catch(Exception ex)
        {
              system.debug('Exception Occured--->'+ex.getMessage());  
        }  
             
    }

}